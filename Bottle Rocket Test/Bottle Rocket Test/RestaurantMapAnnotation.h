//
//  RestaurantMapAnnotation.h
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/30/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface RestaurantMapAnnotation : NSObject<MKAnnotation>

@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * subtitle;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@end
