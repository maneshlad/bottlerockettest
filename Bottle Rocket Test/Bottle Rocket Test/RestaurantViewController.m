//
//  RestaurantTableTableViewController.m
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/29/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import "RestaurantViewController.h"
#import "RestaurantManager.h"
@import CoreLocation;


@interface RestaurantViewController ()

@end

@implementation RestaurantViewController

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self bindData];
    [self bindMap];
}

#pragma mark - Data

- (void)bindData
{
    self.nameLabel.text = self.restaurant.name;
    self.categoryLabel.text = self.restaurant.category;
    self.phoneLabel.text = self.restaurant.formattedPhone;
    self.address1Label.text = self.restaurant.formattedAddress1;
    self.address2Label.text = self.restaurant.formattedAddress2;
    
    if (self.restaurant.twitter == nil)
    {
        self.twitterLabel.text = @"";
    }
    else if([self.restaurant.twitter isEqualToString:@""])
    {
        self.twitterLabel.text = @"";
    }
    else
    {
        self.twitterLabel.text = [NSString stringWithFormat:@"@%@",self.restaurant.twitter];
    }
}

#pragma mark - Map

- (void)bindMap
{
    //background thread, retrieve the Array of Annotations using RestaurantManager
    __block NSArray *annoations;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        Restaurant *record = self.restaurant;
        RestaurantMapAnnotation *temp = [[RestaurantMapAnnotation alloc]init];
        //[temp setTitle:record.name];
        //[temp setSubtitle:record.category];
        [temp setCoordinate:CLLocationCoordinate2DMake([record.lat floatValue], [record.lng floatValue])]; 
        
        annoations = [NSArray arrayWithObject:temp];
        
        //main thread, add the annotations to the map.
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self.mapView showAnnotations:annoations animated:YES];
            
            
        });
    });
}

@end
