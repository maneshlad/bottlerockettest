//
//  LunchMapViewController.m
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/30/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import "LunchMapViewController.h"

@interface LunchMapViewController ()

@end

@implementation LunchMapViewController

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self refreshMap:self];
}

#pragma mark - IB Actions

- (IBAction)refreshMap:(id)sender
{
    //background thread, retrieve the Array of Annotations using RestaurantManager
    __block NSArray *annoations;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        annoations = [self parseRestaurants];
        
        //main thread, add the annotations to the map.
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self.mapView showAnnotations:annoations animated:YES];
        });
    });
}

- (NSMutableArray *)parseRestaurants
{
    NSMutableArray *retval = [[NSMutableArray alloc]init];
    
    for (Restaurant *record in [RestaurantManager sharedInstance].restaurants)
    {
        RestaurantMapAnnotation *temp = [[RestaurantMapAnnotation alloc]init];
        [temp setTitle:record.name];
        [temp setSubtitle:record.category];
        [temp setCoordinate:CLLocationCoordinate2DMake([record.lat floatValue], [record.lng floatValue])];
        [retval addObject:temp];
    }
    return retval;
}

- (IBAction)closeMap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
