//
//  Restaurant.h
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/28/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Restaurant : NSObject

//root properties
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *backgroundImageURL;
@property (strong, nonatomic) UIImage *backgroundImage;

//contact properties
@property (strong, nonatomic) NSString *formattedPhone;
@property (strong, nonatomic) NSString *twitter;

//location properties
@property (strong, nonatomic) NSString *formattedAddress1;
@property (strong, nonatomic) NSString *formattedAddress2;
@property (strong, nonatomic) NSString *lat;
@property (strong, nonatomic) NSString *lng;

+ (NSString*)getCachePath;
- (NSString*)getFilePath;

@end
