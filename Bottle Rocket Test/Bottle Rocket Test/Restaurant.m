//
//  Restaurant.m
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/28/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import "Restaurant.h"

@implementation Restaurant

+ (NSString*)getCachePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    
    BOOL isDir = NO;
    NSError *error;
    if (! [[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:&isDir] && isDir == NO)
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    return cachePath;
}

- (NSString*)getFilePath
{
    NSString *cachePath = [Restaurant getCachePath];
    NSString *fileName = [NSString stringWithFormat:@"%@.%@",self.name,@"png"];
    NSString *filePath =  [cachePath stringByAppendingPathComponent:fileName];
    return filePath;
}

- (id)init
{
    if (self = [super init]) {
        //initialize string variables
        self.name = @"";
        self.category = @"";
        self.backgroundImageURL = @"";
        
        //contact properties
        self.formattedPhone = @"";
        self.twitter = @"";
        
        //location properties
        self.formattedAddress1 = @"";
        self.formattedAddress2 = @"";
        self.lat = @"";
        self.lng = @"";
    }
    return self;
}

@end
