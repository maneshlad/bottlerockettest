//
//  LunchMapViewController.h
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/30/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
#import <MapKit/MapKit.h>
#import "RestaurantMapAnnotation.h"
#import "RestaurantManager.h"

@interface LunchMapViewController : UIViewController<MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

- (IBAction)refreshMap:(id)sender;
- (IBAction)closeMap:(id)sender;

@end
