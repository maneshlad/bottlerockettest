//
//  FirstViewController.m
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/28/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import "LunchViewController.h"

#import "RestaurantViewController.h"
#import "LunchMapViewController.h"
#import "RestaurantCell.h"
#import "Restaurant.h"


@interface LunchViewController ()

@end

@implementation LunchViewController

static NSString * const reuseIdentifier = @"restaurantCell";

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self subscribeToNotification];
    [self fetchData];
    [self configureLayout];
}

- (void)configureLayout
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        // iPad Two Column
        [flowLayout setItemSize:CGSizeMake((self.view.frame.size.width/2), 180)];
    }
    else
    {
        // iPhone Single Column
        [flowLayout setItemSize:CGSizeMake(self.view.frame.size.width, 180)];
    }
    
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setMinimumLineSpacing:0];
    [flowLayout setMinimumInteritemSpacing:0];
    [self.collectionView setCollectionViewLayout:flowLayout];
}

#pragma mark - DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.restaurantManager.restaurants count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RestaurantCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    Restaurant *data = [self.restaurantManager.restaurants objectAtIndex:[indexPath row]];

    cell.nameLabel.text = data.name;
    cell.categoryLabel.text = data.category;
    
    //placeholder image prior to fetching async
    UIImage *placeholder = [UIImage imageNamed:@"cellGradientBackground"];
    cell.backgroundView =  [[UIImageView alloc] initWithImage:placeholder];
    
    UIImageView *imageView = (UIImageView*)cell.backgroundView;
    NSString *imageFile = [data getFilePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:imageFile])
    {
        imageView.image = [UIImage imageWithContentsOfFile:imageFile];
    }
    else
    {
        imageView.alpha = 0;
        
        //Fetch image on a seperate thread
        dispatch_queue_t dnlQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        //queue priority can be HIGH, DEFAULT, LOW
        
        dispatch_async (dnlQueue, ^(void) {
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:data.backgroundImageURL]];             
         
            //render the image on the main thread
            dispatch_async (dispatch_get_main_queue(), ^{
                if (imageData != nil)
                {
                    imageView.image = [UIImage imageWithData:imageData];
                    [cell setNeedsLayout];
                    [imageData writeToFile:imageFile atomically:YES];
                }
                else
                {
                    //download was not successful, set default image or color
                    //cell.backgroundColor = [UIColor lightGrayColor];
                    imageView.image = [UIImage imageNamed:@"cellGradientBackground"];
                    [cell setNeedsLayout];
                }
                //fade in the downloaded image
                [UIView animateWithDuration:0.5 animations:^{ imageView.alpha = 1; }];
                
            });
        });
    }
    
    return cell;
}

- (void)fetchData
{
    self.restaurantManager = [RestaurantManager sharedInstance];
    [self.restaurantManager getData];
}


 #pragma mark - Navigation

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
     
     if ([segue.identifier isEqualToString:@"showRestaurant"])
     {
         NSIndexPath *path = [self.collectionView indexPathsForSelectedItems][0];
         Restaurant *data = [self.restaurantManager.restaurants objectAtIndex:path.row];
         RestaurantViewController *secondViewController = segue.destinationViewController;
         secondViewController.restaurant = data;
         
     }
     else if ([segue.identifier isEqualToString:@"showMap"])
     {
         //Nothing to pass
     }
}

#pragma mark - Notifications

- (void)subscribeToNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishLoadingRestaurants:)
                                                 name:@"RestaurantsLoaded"
                                               object:nil];
}

- (void)didFinishLoadingRestaurants:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"RestaurantsLoaded"])
    {
        //NSLog (@"Successfully received the test notification!");
        [self.collectionView reloadData];
    }
}

- (void)dealloc
{
    //Remove observable
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Rotation

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.collectionView.collectionViewLayout invalidateLayout];
    [self configureLayout];
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    self.collectionView.frame = self.view.bounds;
    [self.collectionView.collectionViewLayout invalidateLayout];
}

@end
