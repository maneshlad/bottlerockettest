//
//  RestaurantCollectionViewCell.h
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/28/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *gradientImageView;

@end
