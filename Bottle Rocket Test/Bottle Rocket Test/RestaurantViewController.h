//
//  RestaurantTableTableViewController.h
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/29/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
#import <MapKit/MapKit.h>
#import "RestaurantMapAnnotation.h"

@interface RestaurantViewController : UIViewController<MKMapViewDelegate>

@property (strong, nonatomic) Restaurant *restaurant;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *twitterLabel;
@property (weak, nonatomic) IBOutlet UILabel *address1Label;
@property (weak, nonatomic) IBOutlet UILabel *address2Label;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

- (void)bindData;

@end
