//
//  RestaurantManager.m
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/28/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import "RestaurantManager.h"


@implementation RestaurantManager

static NSString * const serviceUrl = @"http://sandbox.bottlerocketapps.com/BR_iOS_CodingExam_2015_Server/restaurants.json";

+ (RestaurantManager *)sharedInstance
{
    static RestaurantManager *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        self.restaurants = [[NSMutableArray alloc]init];
    }
    return self;
}

- (Restaurant*)getSampleRestaurant
{
    Restaurant *aRest = [[Restaurant alloc]init];    
    //root properties
    aRest.name = @"Hopdoddy Burger Bar";
    aRest.category = @"Burgers";
    aRest.backgroundImageURL = @"http://sandbox.bottlerocketapps.com/BR_iOS_CodingExam_2015_Server/Images/hopdoddy.png";
    
    //contact properties
    aRest.formattedPhone = @"(972) 387-2337";
    aRest.twitter = @"hopdoddy";
    
    //location properties
    aRest.formattedAddress1 = @"5100 Belt Line Road, STE 502 (Dallas North Tollway)";
    aRest.formattedAddress2 = @"Addison, TX 75254";
    aRest.lat = @"32.950787";
    aRest.lng = @"-96.821118";
    //[aRest fetchImage];
    return aRest;
}

- (void)getData
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL
                                                          URLWithString:serviceUrl]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (!data) {
            NSLog(@"%s: sendAynchronousRequest error: %@", __FUNCTION__, connectionError);
            return;
        } else if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
            NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
            if (statusCode != 200) {
                NSLog(@"%s: sendAsynchronousRequest status code != 200: response = %@", __FUNCTION__, response);
                return;
            }
        }
        
        NSError *parseError = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        if (!dictionary) {
            NSLog(@"%s: JSONObjectWithData error: %@; data = %@", __FUNCTION__, parseError, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            return;
        }
        NSArray *jsonRestaurants = [dictionary valueForKey:@"restaurants"];
        [self parseJsonRestaurants:jsonRestaurants];
        
        //NSLog(@"self.restaurants within manager after parseJsonRestaurants: %lu",(unsigned long)[self.restaurants count]);
        
        //notify other classes that parsing is complete
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"RestaurantsLoaded"
         object:self];
    }];
}

- (void)parseJsonRestaurants:(NSArray*)json
{
    [self.restaurants removeAllObjects];
    
    for(NSDictionary *dict in json)
    {
        Restaurant *aRest = [[Restaurant alloc]init];
            
        //root properties
        if ([dict valueForKey:@"name"] != [NSNull null])
        {
            aRest.name = [dict valueForKey:@"name"];
        }
        
        if ([dict valueForKey:@"category"] != [NSNull null])
        {
            aRest.category = [dict valueForKey:@"category"];
        }
        
        if ([dict valueForKey:@"backgroundImageURL"] != [NSNull null])
        {
            aRest.backgroundImageURL = [dict valueForKey:@"backgroundImageURL"];
        }
        
        //contact properties
        if ([dict valueForKeyPath:@"contact.formattedPhone"] != [NSNull null])
        {
            aRest.formattedPhone = [dict valueForKeyPath:@"contact.formattedPhone"];
        }
        
        if ([dict valueForKeyPath:@"contact.twitter"] != [NSNull null])
        {
            aRest.twitter = [dict valueForKeyPath:@"contact.twitter"];
        }
        
        //location properties
        if ([dict valueForKeyPath:@"location.formattedAddress"] != [NSNull null])
        {
            NSArray *formattedAddress = [dict valueForKeyPath:@"location.formattedAddress"];
            
            aRest.formattedAddress1 = [formattedAddress objectAtIndex:0];
            aRest.formattedAddress2 = [formattedAddress objectAtIndex:1];
        }
        
        if ([dict valueForKeyPath:@"location.lat"] != [NSNull null])
        {
            aRest.lat = [dict valueForKeyPath:@"location.lat"];
        }
        
        if ([dict valueForKeyPath:@"location.lng"] != [NSNull null])
        {
            aRest.lng = [dict valueForKeyPath:@"location.lng"];
        }

        [self.restaurants addObject:aRest];
    }
}

@end
