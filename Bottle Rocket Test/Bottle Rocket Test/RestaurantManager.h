//
//  RestaurantManager.h
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/28/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Restaurant.h"

@interface RestaurantManager : NSObject

@property (strong, nonatomic) NSMutableArray *restaurants;

+ (RestaurantManager *)sharedInstance;
- (void)getData;
- (void)parseJsonRestaurants:(NSArray*)json;
- (Restaurant*)getSampleRestaurant;

@end
