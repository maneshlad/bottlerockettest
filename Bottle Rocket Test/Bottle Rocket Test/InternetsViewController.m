//
//  SecondViewController.m
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/28/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import "InternetsViewController.h"

@interface InternetsViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation InternetsViewController

#pragma mark - View

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self webRefresh:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IB Actions
static NSString * const startingUrl = @"http://www.bottlerocketstudios.com";

- (IBAction)webRefresh:(id)sender
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:startingUrl]];
    [self.webView loadRequest:request];
}

- (IBAction)webGoBack:(id)sender
{
    [self.webView goBack];
}

- (IBAction)webGoFoward:(id)sender
{
    [self.webView goForward];
}

@end
