//
//  AppDelegate.m
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/28/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //Setup Appearance of tabs, tabbar, toolbar
    UIColor *normalColor = [UIColor colorWithRed:151.0f/255.0f
                                           green:151.0f/255.0f
                                            blue:151.0f/255.0f
                                           alpha:1.0f];
    
    UIColor *selectedColor = [UIColor colorWithRed:255.0f/255.0f
                                             green:255.0f/255.0f
                                              blue:255.0f/255.0f
                                             alpha:1.0f];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"AvenirNext-Regular" size:10.0f], NSFontAttributeName,  normalColor, NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"AvenirNext-Regular" size:10.0f], NSFontAttributeName,  selectedColor, NSForegroundColorAttributeName,nil] forState:UIControlStateSelected];
    
    UIColor *greenTintColor = [UIColor colorWithRed:67.0f/255.0f
                                           green:232.0f/255.0f
                                            blue:149.0f/255.0f
                                           alpha:1.0f];

    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UIToolbar appearance] setBackgroundColor:greenTintColor];
    return YES;
}

@end
