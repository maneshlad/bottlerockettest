//
//  FirstViewController.h
//  Bottle Rocket Test
//
//  Created by Manesh Lad on 4/28/15.
//  Copyright (c) 2015 Manesh Lad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantManager.h"

@interface LunchViewController : UICollectionViewController

@property (strong, nonatomic) RestaurantManager *restaurantManager;

@end

